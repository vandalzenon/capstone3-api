const jwt = require("jsonwebtoken");
const dotenv = require("dotenv").config();

const JWT_VALUE = process.env.JWT_VALUE;

module.exports.createToken = (data) => {
  let user = {
    name: data.fullName,
    role: data.role,
    id: data._id,
  };
  return jwt.sign(user, JWT_VALUE, {});
};

module.exports.verify = (req, res, next) => {
  let token = req.headers.authorization;
  if (typeof token === "undefined") {
    return res.send("Forbidden");
  } else {
    token = token.split(" ");
    let accessToken = token[1];
    jwt.verify(accessToken, JWT_VALUE, (error, decodedToken) => {
      if (error) {
        return res.send("Forbidden Action");
      } else {
        req.user = decodedToken;
        return next();
      }
    });
    return;
  }
};

module.exports.verifySeller = (req, res, next) => {
  if (req.user.role == "seller") {
    return next();
  }
  res.send("Forbidden Action");
};
module.exports.verifyAdmin = (req, res, next) => {
  if (req.user.role === "admin") {
    return next();
  }
  res.send("Forbidden Action");
};
