const bcrypt = require("bcrypt");
const dotenv = require("dotenv").config();
const User = require("./../models/Users");
const { verify, verifyAdmin, createToken } = require("./../authorization");


const SALT = Number(process.env.SALT);


module.exports.register = async (data) =>{
    const {email,password,firstName,lastName} = data;
    
    const isEmailUsed = await User.findOne({email:email}).then(emails =>{
        if(emails) return true;
        return false;
    })

    if (isEmailUsed === true){
        return 'Email is already used';
    }
    let newUser = new User({
        firstName,
        lastName,
        fullName : `${firstName} ${lastName}`,
        password: bcrypt.hashSync(password,SALT),
        email
    })
    return newUser.save().then(registeredUser =>{
        if (registeredUser) return registeredUser;
    }).catch(err => err.message)

}
module.exports.login = (data) =>{
    const {email, password} = data;
    return User.findOne({email:email}).then(foundUser=>{
        if (foundUser) {
            const hashedPassword = foundUser.password;
            
           return bcrypt.compare(password, hashedPassword).then(passwordResult =>{
                if (passwordResult === false) return 'Wrong Credentials'
                return createToken(foundUser);
            }).catch(err => err.message)
        };
        return 'Wrong Credentials'  
    })
}

module.exports.getAllUsers = () =>{
    return User.find({}).then(foundUsers=>{
        return foundUsers;
    }).catch(err => err.message)
}

module.exports.accountInfo = (userId) =>{
    return User.findById(userId).then(foundCurrentUser =>{return foundCurrentUser}).catch(err =>err.message)
}

module.exports.updateRoles = async (userId,role) =>{
    let newRole = {
        role: role
    }
    const isRoleTheSame = await User.findById(userId).then(foundUser =>{
        if (foundUser.role === role){
            return true
        }
        return false
    })

    if(isRoleTheSame === true){
        return `User is already set to ${role}`
    }


    return User.findByIdAndUpdate(userId,newRole).then(updatedUser=>{
        if (updatedUser){
            return 'User role has been updated'
        } 
    }).catch(err=>err.message)
}
module.exports.archiveUser =  (userId) =>{
    let activeStatus= {
        isActive: false
    }
     return User.findByIdAndUpdate(userId,activeStatus).then(foundUser =>{
        if(foundUser) return 'User is Archived'
    }).catch(err=>err.message)
    
}
module.exports.restoreUser = (userId) =>{
    let activeStatus= {
        isActive: true
    }
     return User.findByIdAndUpdate(userId,activeStatus).then(foundUser =>{
        if(foundUser) return 'User is Restored'
    }).catch(err=>err.message)
}
module.exports.deleteAccount = (userId) =>{
    return User.findByIdAndDelete(userId).then(deletedAccount =>{
        if(deletedAccount) return 'Account Successfully Deleted';
        
    }).catch(err => err.message)
}