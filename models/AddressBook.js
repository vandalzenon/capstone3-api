const  mongoose =  require ("mongoose");

const addressBookSchema = new mongoose.Schema({
    fullName:{
        type:String,
        required: [true, "fullName is required"]
    },provinceId:{
        type:Number,
        required: [true, "provinceId is required"]
    },municipalId:{
        type:Number,
        required: [true, "municipalId is required"]
    },barangayId:{
        type:Number,
        required: [true, "barangayId is required"]
    },
    streetHouseNumber:{
        type:String,
        required: [true, "streetHouseNumber is required"]
    },
    isHome:{
        type:Boolean,
        default:true
    }
})

const AddressBook =  mongoose.model();
module.exports = AddressBook;