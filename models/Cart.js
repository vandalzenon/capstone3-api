const mongoose = require("mongoose");

const cartSchema = new mongoose.Schema({
    product:[{
        productId:{
            type:String,
            required:[true, 'productId is required']
        },
        productQuantity:{
            type:Number,
            required:[true, 'productQuantity is required']
        }
        
    }]
})

const Cart = mongoose.model("Cart",cartSchema);
module.exports = Cart;
