const mongoose = require("mongoose");


new orderSchema = new mongoose.Schema({
    totalAmount:{
        type:Number
    },
    productId:{
        type:String,
        required:[true, 'productId is required']
    },
    productQuantity:{
        type:Number,
        required:[true, 'productId is required']
    },
    status:{
        type:String,
       default:"Processing"
    },
    review:[{
        reviewRate :{
            type:Number,
            default:5
        },
        reviewMessage:{
            type:String,
            default:""
        }
    }]
})

const Order = mongoose.model("Order",orderSchema);
module.exports =Order;