const  mongoose =  require("mongoose");


const  bannerSchema = new mongoose.Schema({
    image:{
        type:String,
        default: 'image in Production'
    }
})

const Banner  = mongoose.model("Banner",bannerSchema);
module.exports = Banner;