const mongoose = require('mongoose');



const userSchema = new mongoose.Schema({
    firstName : {
        type: String,
        required : [true,'firstName is required']
    },
    lastName: {
        type:String,
        required: [true, 'lastName is required']
    },
    fullName:{
        type:String,
        
    },
    isActive:{
        type:Boolean,
        default:true
    },
    email:{
        type:String,
        required:[true , 'email is required']
    },
    password:{
        type:String,
        required:[true, 'password is required']
    },
     defaultAdressId:{
        type:String,
        
    },
    adressBook : [{
       
        addressId:{
            type:String,
            required:[true, 'defaultAdressId is required']
        }
    }],
    order:[{
        orderId:{
            type:String,
            required:[true, 'orderId is required']
        }
    }],
    cartId:{
        type:String,
       
    },
    isActive:{
        type:Boolean,
        default:true
    },
    role:{
        type:String,
        default:"user"
    },
  
    
})

const User =  mongoose.model("User",userSchema);
module.exports = User;