const mongoose =  require("mongoose");



const  saleSchema =  new mongoose.Schema({
    productId : {
        type:String,
        required: [true,"productId is required"]
    },
    discount:{
        type:Number,
        required: [true,"discount is required"]
    },
    discounted:{
        type: Number
    }
})


const Sale =  mongoose.model("Sale", saleSchema);
module.exports = Sale;