const mongoose =  require("mongoose");


const storeSchema = new mongoose.Schema({
    addressId:{
        type:String,
        required:[true,  "addressId is required"]
    },name:{
        type:String,
        required:[true,  "name is required"]
    },
    products:[{
        productId: {
            type:String,
            required:[true,  "productId is required"]
        }
    }]
})