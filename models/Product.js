const mongoose = require('mongoose');


const ProductSchema = new mongoose.Schema({
    name: {
        type:String,
        required: [true, 'name is required']
    },price:{
        type:Number,
        required: [true, 'name is required']
    },
    isActive:{
        type:Boolean,
        default:true
    },
    reviewRate:{
        type:Number,
        default:0
    },
    description:{
        type:String,
        required: [true, 'description is required']
    },
    categories:[{
        categoryId:{
            type:String,
            required: [true, 'categoryId is required']
        }
    }]
})


const Product = mongoose.model("Product", ProductSchema);
module.exports = Product;