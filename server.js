const express = require('express');
const mongoose = require('mongoose');
const dotenv = require('dotenv').config();

const addressRoute = require('./source/address')
const userRoute = require('./routes/user')


const DB_STRING = process.env.DB_STRING;
const PORT = process.env.PORT;

const app = express();
app.use(express.json());
app.use("/address",addressRoute);
app.use("/users",userRoute);

mongoose.connect(DB_STRING);
const mongoooseStatus = mongoose.connection;
mongoooseStatus.once('open',()=>{
    console.log("\x1b[33m",`Database Connected`)
})

app.listen(PORT,()=>{console.log("\x1b[36m",`App is running on PORT :\x1b[35m ${PORT}`)})


app.get("/", (req,res)=>{
    res.send(`Welcome to Zenon's e-Commerce API`);
})