const controller = require("./../controller/user");

const route = require("express").Router();
const {verify,verifyAdmin} =require("./../authorization")

route.post('/register', (req,res)=>{
    const data =  req.body;
    controller.register(data).then(result =>{
        res.send(result)
    })
})

route.post('/login',(req,res)=>{
    const data = req.body;
    controller.login(data).then(result =>{
        res.send(result)
    })
});

route.get('/all',verify,verifyAdmin,(req,res)=>{
   controller.getAllUsers().then(result =>{
       res.send(result)
   })
})


route.get('/user',verify,(req,res)=>{
    const userId =  req.user.id;
    controller.accountInfo(userId).then(result =>{
        res.send(result);
    }) 
    
})
route.put('/roleUpdate',verify,verifyAdmin,(req,res)=>{
    const {userId,role} = req.body;
    controller.updateRoles(userId, role).then(result => {
        res.send(result);
    })
})
route.put('/archive',verify,verifyAdmin,(req,res)=>{
    const {userId} = req.body;
    controller.archiveUser(userId).then(result =>{
        res.send(result);
    })
    
})
route.put('/restore',verify,verifyAdmin,(req,res)=>{
    const {userId} = req.body;
    controller.restoreUser(userId).then(result =>{
        res.send(result);
    })
    
})

route.delete('/delete',verify,(req,res)=>{
    controller.deleteAccount(req.user.id).then(result =>{
        res.send(result);
    })
})


module.exports = route;